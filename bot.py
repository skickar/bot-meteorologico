#-*- coding: utf-8 -*-
from telegram.ext import Updater
from telegram.ext import CommandHandler, CallbackQueryHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext.dispatcher import run_async
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import requests, json 
import threading
import time
import datetime
import os
#Variables globales de datos meteorlogicos
tiempo_ultima_actualizacion = 0
mensaje = "Sin datos por el momento"
mensaje_prediccion = "Sin datos de predicción por el momento"

boton_actualizar = InlineKeyboardButton(
    text='actualizar', # text that show to user
    callback_data="/tiempo_actual" # text that send to bot when user tap button
    )

def start(bot, update):
	""" This function will be executed when '/start' command is received """
	mensaje_start = ("Utiliza el comando '/tiempo_actual' para ver la informacion meteorologica"
					" y /prediccion para una prediccion del tiempo de las proximas horas")
	bot.send_message(chat_id=update.message.chat_id, text=mensaje_start)

def ultima_actualizacion(bot, update):
	""" This function will be executed when '/ultima_actualizacion' command is received """
	bot.send_message(chat_id=update.message.chat_id, text=str(tiempo_ultima_actualizacion))


def actualizar_tiempo_prediccion():
	while True:
		complete_url = "http://api.openweathermap.org/data/2.5/forecast?id=3104324&appid=3974461a1f67f51495d6c273db02ccaf&lang=es"

		response = requests.get(complete_url)

		json_data = response.json()

		global mensaje_prediccion
		mensaje_prediccion = ''
		# Iterates through the array of dictionaries named list in json_data
		i=0
		for item in json_data["list"]:
			if i>=10:
				break
			else:
				# Time of the weather data received, partitioned into 3 hour blocks
				unixtimestamp = item["dt"]

				hora_prediccion = datetime.datetime.fromtimestamp(unixtimestamp).strftime('%d/%m/%Y - %H:%M')
				
				main = item["main"]

				#Stores temperature pressure and humidity data for each forecast
				prediccion_temp = main["temp"]
				prediccion_pressure = main["pressure"]
				prediccion_humidity = main["humidity"]

				#Stores wheater description for each forecast
				weather = item["weather"]
				prediccion_descripcion = weather[0]["description"]

				#Stores wind data for each forecast
				wind = item["wind"]
				prediccion_wind = wind["speed"]

				#Generate the message of forecasat with all data available
				mensaje_prediccion += ("\n*Predicción para: " + str(hora_prediccion) + "*" 
									"\n\U0001F525 Temperatura = " + str(round(prediccion_temp-273,2)) + "ºC"
									"\n\U0001F4A8 Viento = " + str(round(prediccion_wind*3.6,1)) + "km/s"
									"\n\U0001F4A7 Humedad = " + str(prediccion_humidity) + "%"
									"\nPresión atmosférica = " + str(int(prediccion_pressure)) + "hPa"
									"\nDescripción = " + str(prediccion_descripcion) + "\n")
				i = i+1
		#print datetime of last update
#		print("prediccion actualizada: ")
#		print(datetime.datetime.now().strftime("%d/%m/%Y - %H:%M"))
		time.sleep(10*60)



def actualizar_tiempo_actual():
	while True:
		# Python program to find current  
		# weather details of any city 
		# using openweathermap api 

		# complete_url variable to store 
		# complete url address 
		complete_url = "http://api.openweathermap.org/data/2.5/weather?id=3104324&appid=3974461a1f67f51495d6c273db02ccaf&lang=es"

		# get method of requests module 
		# return response object 
		response = requests.get(complete_url) 

		# json method of response object  
		# convert json format data into 
		# python format data 
		x = response.json() 

		# Now x contains list of nested dictionaries 
		# Check the value of "cod" key is equal to 
		# "404", means city is found otherwise, 
		# city is not found 
		if x["cod"] != "404": 

			# store the value of "main" 
			# key in variable y 
			y = x["main"] 

			# store the value corresponding 
			# to the "temp" key of y
			current_temperature = y["temp"]

			# store the value corresponding 
			# to the "pressure" key of y 
			current_pressure = y["pressure"] 

			# store the value corresponding 
			# to the "humidity" key of y 
			current_humidity = y["humidity"] 

			# store the value of "wind"
			# key in variable w
			w = x["wind"]

			# store the value corresponding 
			# to the "wind.speed" key of w
			current_wind = w["speed"]

			# store the value of "weather" 
			# key in variable z 
			z = x["weather"] 

			# store the value corresponding  
			# to the "description" key at  
			# the 0th index of z 
			weather_description = z[0]["description"] 

			#bot.send_message(chat_id=update.message.chat_id, text=mensaje)

		else: 
			error = "Ciudad no encontrada"
			#bot.send_message(chat_id=update.message.chat_id, text=error)
		
		
		
		global tiempo_ultima_actualizacion
		tiempo_ultima_actualizacion = datetime.datetime.now().strftime("%d/%m/%Y - %H:%M")
#		print("Datos actualizados con fecha: ")
#		print(tiempo_ultima_actualizacion)
		#Crea el mensaje a mostrar posteriormente
		global mensaje
		mensaje = (	"*Datos obtenidos con fecha: " + str(tiempo_ultima_actualizacion) + "*"
				"\n\U0001F525 Temperatura = " + str(round(current_temperature-273,2)) + "ºC"
				"\n\U0001F4A8 Viento = " + str(round(current_wind*3.6,1)) + "km/s"
				"\n\U0001F4A7 Humedad = " + str(current_humidity) + "%"
				"\nPresión atmosférica = " + str(current_pressure) + "hPa"
				"\nDescripción = " + str(weather_description))
		
		#El servidor de openweather actualiza sus datos cada diez minutos,
		#por lo que se solicitan datos cada diez minutos
		time.sleep(10*60)

def prediccion(bot, update):
	""" This function will be executed when '/prediccion' command is received """
	bot.send_message(chat_id=update.message.chat_id, text=str(mensaje_prediccion),parse_mode="markdown")


def tiempo_actual(bot, update):
	""" This function will be executed when '/tiempo_actual' command is received """

	#bot.send_message(chat_id=update.message.chat_id, text=str(mensaje))
	keyboard = [[InlineKeyboardButton("Actualizar",callback_data="patata")]]
	replyMarkup = InlineKeyboardMarkup(keyboard)

	update.message.reply_text(str(mensaje), parse_mode="markdown", reply_markup=replyMarkup)

def button(bot, update):
	query = update.callback_query
	bot.answerCallbackQuery(query.id)

	keyboard = [[InlineKeyboardButton("Actualizar",callback_data="patata")]]

	replyMarkup = InlineKeyboardMarkup(keyboard)

	bot.edit_message_text(text=str(mensaje),
							chat_id=query.message.chat_id,
	 						message_id=query.message.message_id,
							parse_mode="markdown",
							reply_markup=replyMarkup)




def main(bot_token):
	""" Main function of the bot """
	#Espera para que se inicie la red
	time.sleep(30)
	updater = Updater(token=bot_token)
	dispatcher = updater.dispatcher

	#Actualiza los datos cada 10 minutos
	z = threading.Thread(target=actualizar_tiempo_actual)
	p = threading.Thread(target=actualizar_tiempo_prediccion)
	z.start()
	p.start()

	# Command handlers
	start_handler = CommandHandler('start', start)
	tiempo_actual_handler = CommandHandler('tiempo_actual', tiempo_actual)
	ultima_actualizacion_handler = CommandHandler('ultima_actualizacion', ultima_actualizacion)
	prediccion_handler = CommandHandler('prediccion', prediccion)
	updater.dispatcher.add_handler(CallbackQueryHandler(button))

	# Add the handlers to the bot
	dispatcher.add_handler(start_handler)
	dispatcher.add_handler(tiempo_actual_handler)
	dispatcher.add_handler(ultima_actualizacion_handler)
	dispatcher.add_handler(prediccion_handler)

	# Starting the bot
	updater.start_polling(allowed_updates=[])

if __name__ == "__main__":
	main(os.environ["token_bot_meteorologico"])
